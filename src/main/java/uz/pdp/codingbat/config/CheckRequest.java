package uz.pdp.codingbat.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.repository.UserRepository;
import uz.pdp.codingbat.util.ContextHolder;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CheckRequest extends OncePerRequestFilter {
    final UserRepository userRepository;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorization = request.getHeader("authorization");
        if (authorization == null){
            return;
        }
        Optional<User> optionalUser = userRepository.findById(authorization);
        optionalUser.ifPresent(user -> ContextHolder.user = user);
        filterChain.doFilter(request, response);
    }
}
