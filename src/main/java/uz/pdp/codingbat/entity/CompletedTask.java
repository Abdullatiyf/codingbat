package uz.pdp.codingbat.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity(name = "completed_task")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompletedTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private User users;
    @ManyToOne
    private Task task;
    private Boolean isCompleted;
    private String answerText;
    private Integer testCaseCount;
    private Integer successTestCaseCount;
    private Boolean isDeleted;
    private LocalDateTime date;
}
