package uz.pdp.codingbat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Boolean success;
    private String userId;
    private String message;
    private Object object;

    public UserDTO(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public UserDTO(Boolean success, String userId, String message) {
        this.success = success;
        this.userId = userId;
        this.message = message;
    }

    public UserDTO(Boolean success, Object object) {
        this.success = success;
        this.object = object;
    }
}
