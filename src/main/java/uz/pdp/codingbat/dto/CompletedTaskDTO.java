package uz.pdp.codingbat.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.entity.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompletedTaskDTO {
    private User user;
    private Task task ;
    private Boolean isCompleted;
    private String answerText;
    private Integer testCaseCount;
    private Integer successTestCaseCount;
    private Boolean isDeleted;
}
