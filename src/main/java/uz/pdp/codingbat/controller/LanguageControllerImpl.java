package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.entity.Language;
import uz.pdp.codingbat.service.LanguageService;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
@RestController
@RequiredArgsConstructor
public class LanguageControllerImpl implements LanguageController{
    final LanguageService languageService;
    @Override
    public ResultMessage add(Language language) {
        return languageService.add(language);
    }

    @Override
    public Language show(Integer id) {
        return languageService.show(id);
    }

    @Override
    public List<Language> showAll() {
        return languageService.showAll();
    }

    @Override
    public ResultMessage edit(Integer id, Language language) {
        return languageService.edit(id,language);
    }

    @Override
    public ResultMessage delete(Integer id) {
        return languageService.delete(id);
    }
}
