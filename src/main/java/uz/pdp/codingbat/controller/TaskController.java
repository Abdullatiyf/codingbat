package uz.pdp.codingbat.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequestMapping("/task")
public interface TaskController {
    @PostMapping
    ResultMessage addTask(@RequestBody Task task);

    @GetMapping("/{id}")
    Task getTaskById(@PathVariable Integer id);

    @GetMapping
    List<Task> getAllTasks();

    @GetMapping("/category/{id}")
    List<Task> getAllTasksByCategory(@PathVariable Integer id);

    @PutMapping("/{id}")
    ResultMessage editTask(@PathVariable Integer id, @RequestBody Task task);

    @DeleteMapping("/{id}")
    ResultMessage deleteTask(@PathVariable Integer id);
}
