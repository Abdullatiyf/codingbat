package uz.pdp.codingbat.controller;


import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.dto.TestCaseDTO;
import uz.pdp.codingbat.entity.TestCase;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequestMapping(path = "/testCase")
public interface TestCaseController {
    @PostMapping
    ResultMessage createTestCase(@RequestBody TestCaseDTO testCaseDTO);

    @GetMapping("/{id}")
    TestCase getTestCase(@PathVariable Integer id);

    @GetMapping("/active")
    List<TestCase> getActiveTestCases();

    @GetMapping("deleted")
    List<TestCase> getDeletedTestCases();

    @GetMapping("/problem/{id}")
    List<TestCase> getTestCaseBelongTask(@PathVariable Integer id);

    @PutMapping("/{id}")
    ResultMessage updateTestCase(@RequestBody TestCaseDTO testCaseDTO, @PathVariable Integer id);

    @DeleteMapping("/{id}")
    ResultMessage deleteTestCase(@PathVariable Integer id);

}

