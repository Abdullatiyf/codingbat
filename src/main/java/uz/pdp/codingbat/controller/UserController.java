package uz.pdp.codingbat.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.dto.UserDTO;
import uz.pdp.codingbat.util.ResultMessage;

@RequestMapping(path = "/user")
public interface UserController {
    @PostMapping ("/signUp")
    UserDTO signUp(@RequestBody User user);

    @GetMapping("/{email}/{id}/{code}")
    ResultMessage validateUser(@PathVariable String email, @PathVariable String id, @PathVariable String code);

    @PostMapping("/signIn")
    ResultMessage checkUser(@RequestBody User user);

    @GetMapping("/{id}")
    UserDTO showUser(@PathVariable String id);

    @GetMapping
    UserDTO showUsers();

    @PutMapping("/{id}")
    ResultMessage editUser(@PathVariable String id, @RequestBody User user);

    @DeleteMapping("/{id}")
    ResultMessage deleteUser(@PathVariable String id);
}
