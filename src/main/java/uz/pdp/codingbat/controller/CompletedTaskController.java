package uz.pdp.codingbat.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.dto.CompletedTaskDTO;
import uz.pdp.codingbat.entity.CompletedTask;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequestMapping(value = "/ct")
public interface CompletedTaskController {
    @GetMapping
    List<CompletedTask> getAllCompletedTasks();

    @GetMapping("/{id}")
    CompletedTask getCompletedTask(@PathVariable Integer id);

    @GetMapping(value = "all")
    List<CompletedTask> getOnlyCompleted();

    @PostMapping
    ResultMessage create(@RequestBody CompletedTaskDTO completedTaskDTO);

    @PutMapping(value = "/new/{id}")
    ResultMessage update(@RequestBody CompletedTaskDTO completedTaskDTO, @PathVariable Integer id);

    @DeleteMapping(value = "/delete/{id}")
    ResultMessage delete(@PathVariable Integer id);
}
