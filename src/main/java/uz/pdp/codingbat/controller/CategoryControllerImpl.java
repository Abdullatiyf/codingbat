package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.entity.Category;
import uz.pdp.codingbat.service.CategoryService;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
@RestController
@RequiredArgsConstructor
public class CategoryControllerImpl implements CategoryController{
    final CategoryService categoryService;
    @Override
    public ResultMessage add(Category category) {
        return categoryService.add(category);
    }

    @Override
    public Category show(Integer id) {
        return categoryService.show(id);
    }

    @Override
    public List<Category> showAll() {
        return categoryService.showAll();
    }

    @Override
    public List<Category> getAllByLanguage(Integer id) {
        return categoryService.getAllByLanguage(id);
    }

    @Override
    public ResultMessage edit(Integer id, Category category) {
        return categoryService.edit(id,category);
    }

    @Override
    public ResultMessage delete(Integer id) {
        return categoryService.delete(id);
    }
}

