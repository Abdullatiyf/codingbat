package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.dto.TestCaseDTO;
import uz.pdp.codingbat.entity.TestCase;
import uz.pdp.codingbat.service.TestCaseService;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class TestCaseControllerImpl implements TestCaseController{
    final TestCaseService testCaseService;

    @Override
    public ResultMessage createTestCase(TestCaseDTO testCaseDTO) {
        return testCaseService.createTestCase(testCaseDTO);
    }

    @Override
    public TestCase getTestCase(Integer testCaseId) {
        return testCaseService.getTestCase(testCaseId);
    }

    @Override
    public List<TestCase> getActiveTestCases() {
        return testCaseService.getActiveTestCases();
    }

    @Override
    public List<TestCase> getDeletedTestCases() {
        return testCaseService.getDeletedTestCases();
    }

    @Override
    public List<TestCase> getTestCaseBelongTask(Integer taskId) {
        return testCaseService.getTestCaseBelongTask(taskId);
    }

    @Override
    public ResultMessage updateTestCase(TestCaseDTO testCaseDTO, Integer tesCaseId) {
        return testCaseService.updateTestCase(testCaseDTO,tesCaseId);
    }

    @Override
    public ResultMessage deleteTestCase(Integer tesCaseId) {
        return testCaseService.deleteTestCase(tesCaseId);
    }
}
