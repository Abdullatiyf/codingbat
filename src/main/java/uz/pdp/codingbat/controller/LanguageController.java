package uz.pdp.codingbat.controller;

import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.entity.Language;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequestMapping("/language")
public interface LanguageController {
    @PostMapping
    ResultMessage add(@RequestBody Language language);
    @GetMapping("/{id}")
    Language show(@PathVariable Integer id);
    @GetMapping
    List<Language> showAll();
    @PutMapping("/{id}")
    ResultMessage edit(@PathVariable Integer id,@RequestBody Language language);
    @DeleteMapping("/{id}")
    ResultMessage delete(@PathVariable Integer id);
}
