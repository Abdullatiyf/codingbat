package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.service.TaskService;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class TaskControllerImpl implements TaskController {
    final TaskService taskService;

    @Override
    public ResultMessage addTask(@RequestBody Task task) {
        return taskService.add(task);
    }

    @Override
    public Task getTaskById(@PathVariable Integer id) {
        return taskService.show(id);
    }

    @Override
    public List<Task> getAllTasks() {
        return taskService.showAll();
    }

    @Override
    public List<Task> getAllTasksByCategory(Integer id) {
        return taskService.getAllTasksByCategory(id);
    }

    @Override
    public ResultMessage editTask(Integer id, Task task) {
        return taskService.edit(id, task);
    }

    @Override
    public ResultMessage deleteTask(@PathVariable Integer id) {
        return taskService.delete(id);
    }
}
