package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.dto.UserDTO;
import uz.pdp.codingbat.service.UserService;
import uz.pdp.codingbat.util.ResultMessage;

@RestController
@RequiredArgsConstructor
public class UserControllerImpl implements UserController{
    final UserService userService;

    @Override
    public UserDTO signUp(User user) {
        return userService.signUp(user);
    }

    @Override
    public ResultMessage validateUser(String email, String id, String code) {
        return userService.validateUser(email, id, code);
    }

    @Override
    public ResultMessage checkUser(User user) {
        return userService.checkUser(user);
    }

    @Override
    public UserDTO showUser(String id) {
        return userService.showUser(id);
    }

    @Override
    public UserDTO showUsers() {
        return userService.showUsers();
    }

    @Override
    public ResultMessage editUser(String id, User user) {
        return userService.editUser(id, user);
    }

    @Override
    public ResultMessage deleteUser(String id) {
        return userService.deleteUser(id);
    }
}
