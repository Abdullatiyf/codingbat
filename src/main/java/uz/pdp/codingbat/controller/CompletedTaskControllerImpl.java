package uz.pdp.codingbat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.codingbat.dto.CompletedTaskDTO;
import uz.pdp.codingbat.entity.CompletedTask;
import uz.pdp.codingbat.service.CompletedTaskService;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CompletedTaskControllerImpl implements CompletedTaskController {
    final CompletedTaskService completedTaskService;

    @Override
    public List<CompletedTask> getAllCompletedTasks() {
        return completedTaskService.getAllCompletedTasks();
    }

    @Override
    public CompletedTask getCompletedTask(Integer id) {
        return completedTaskService.getCompletedTask(id);
    }

    @Override
    public List<CompletedTask> getOnlyCompleted() {
        return completedTaskService.getAllByIsComplete();
    }

    @Override
    public ResultMessage create(CompletedTaskDTO completedTaskDTO) {
        return completedTaskService.create(completedTaskDTO);
    }

    @Override
    public ResultMessage update(CompletedTaskDTO completedTaskDTO, Integer id) {
        return completedTaskService.update(id, completedTaskDTO);
    }

    @Override
    public ResultMessage delete(Integer id) {
        return completedTaskService.delete(id);
    }
}

