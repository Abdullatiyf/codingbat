package uz.pdp.codingbat.controller;


import org.springframework.web.bind.annotation.*;
import uz.pdp.codingbat.entity.Category;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

@RequestMapping("/category")
public interface CategoryController {
    @PostMapping
    ResultMessage add(@RequestBody Category category);
    @GetMapping("/{id}")
    Category show(@PathVariable Integer id);
    @GetMapping
    List<Category> showAll();
    @GetMapping("/lan/{id}")
    List<Category> getAllByLanguage(@PathVariable Integer id);
    @PutMapping("/{id}")
    ResultMessage edit(@PathVariable Integer id,@RequestBody Category category);
    @DeleteMapping("/{id}")
    ResultMessage delete(@PathVariable Integer id);
}
