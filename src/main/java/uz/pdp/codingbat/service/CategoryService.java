package uz.pdp.codingbat.service;

import uz.pdp.codingbat.entity.Category;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

public interface CategoryService {
    ResultMessage add(Category category);
    Category show(Integer id);
    List<Category> showAll();
    List<Category> getAllByLanguage(Integer id);
    ResultMessage edit(Integer id, Category category);
    ResultMessage delete(Integer id);


}
