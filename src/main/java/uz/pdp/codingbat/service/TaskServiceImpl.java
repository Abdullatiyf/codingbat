package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.codingbat.entity.Category;
import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.repository.CategoryRepository;
import uz.pdp.codingbat.repository.TaskRepository;
import uz.pdp.codingbat.util.ContextHolder;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService{
    final TaskRepository taskRepository;
    final CategoryRepository categoryRepository;

    @Override
    public ResultMessage add(Task task) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Task checkTask = checkTask(task.getName());
        if (Objects.nonNull(checkTask)){
            return new ResultMessage(false, "Task added before!!!");
        }
        Optional<Category> optionalCategory = categoryRepository.findById(task.getCategory().getId());
        if (optionalCategory.isPresent()) {
            task.setIsDeleted(false);
            task.setCategory(optionalCategory.get());
            taskRepository.save(task);
            return new ResultMessage(true, "Task saved");
        }
        return new ResultMessage(false, "Category not found");
    }

    private Task checkTask(String taskName) {
        List<Task> tasks = taskRepository.findAll();
        return tasks.stream().filter(task -> Objects.equals(task.getName(), taskName))
                .findAny().orElse(null);
    }
    @Override
    public Task show(Integer id) {
        return taskRepository.findById(id).orElse(null);
    }
    @Override
    public List<Task> showAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> getAllTasksByCategory(Integer id) {
        return taskRepository.findAllByIsDeletedFalseAndCategory_Id(id);
    }

    @Override
    public ResultMessage edit(Integer id, Task task) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Optional<Task> taskOptional = taskRepository.findById(id);
        Optional<Category> optionalCategory = categoryRepository.findById(task.getCategory().getId());
        if (taskOptional.isPresent()) {
            Task task1 = taskOptional.get();
            if (optionalCategory.isPresent()) {
                task1.setName(task.getName());
                task1.setQuestion(task.getQuestion());
                task1.setTemplate(task.getTemplate());
                task1.setCategory(optionalCategory.get());
                taskRepository.save(task1);
                return new ResultMessage(true, "Edited");
            }
            return new ResultMessage(false, "Category not found");
        }
        return new ResultMessage(false, "Task not found");
    }


    @Override
    public ResultMessage delete(Integer id) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            task.setIsDeleted(true);
            taskRepository.save(task);
            return new ResultMessage(true, "Deleted");
        }
        return new ResultMessage(false, "Task not found");
    }
}
