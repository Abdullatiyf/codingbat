package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.dto.UserDTO;
import uz.pdp.codingbat.repository.UserRepository;
import uz.pdp.codingbat.util.ContextHolder;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    final UserRepository userRepository;

    @Override
    public UserDTO signUp(User user) {
        if (checkEmail(user.getEmail()) != null){
            return new UserDTO(false, "Email is already taken");
        }
        user.setRole("user");
        user.setIsDeleted(false);
        user.setIsActive(false);
        user.setSecurityCode(UUID.randomUUID().toString());
        User savedUser = userRepository.save(user);
        return new UserDTO(true, savedUser.getId(),
                "http://localhost:8080/user/"+savedUser.getEmail()+"/"+savedUser.getId()+"/"+savedUser.getSecurityCode());
    }

    private User checkEmail(String email) {
        List<User> users = userRepository.findAll();
        return users.stream().filter(user -> user.getEmail().equals(email))
                .findAny().orElse(null);
    }

    @Override
    public ResultMessage validateUser(String email, String id, String code) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            if (Objects.equals(user.getSecurityCode(), code)){
                user.setIsActive(true);
                userRepository.save(user);
                return new ResultMessage(true, "Success");
            }
            return new ResultMessage(false, "Validation failed");
        }
        return new ResultMessage(false, "User not found");
    }

    @Override
    public ResultMessage checkUser(User user) {
        Optional<User> optionalUser = userRepository.checkUser(user.getEmail(), user.getPassword());
        if (optionalUser.isPresent()){
            return new ResultMessage(true, "Welcome to cabinet");
        }
        return new ResultMessage(false, "User not found");
    }

    @Override
    public UserDTO showUser(String id) {
        if (Objects.equals(ContextHolder.user.getRole(),"admin")) {
            Optional<User> optionalUser = userRepository.findById(id);
            return optionalUser.map(user -> new UserDTO(true, user)).orElseGet(() -> new UserDTO(false, "User not found"));
        }
        return new UserDTO(false, "Permission denied");
    }

    @Override
    public UserDTO showUsers() {
        if (Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new UserDTO(true, userRepository.findAllByIsDeletedFalse());
        }
        return new UserDTO(false, "Permission denied");
    }

    @Override
    public ResultMessage editUser(String id, User user) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        User user1 = checkEmail(user.getEmail());
        if (user1 == null){
            Optional<User> optionalUser = userRepository.findById(id);
            if (optionalUser.isPresent()){
                User currentUser = optionalUser.get();
                currentUser.setEmail(user.getEmail());
                currentUser.setPassword(user.getPassword());
                currentUser.setRole(user.getRole());
                userRepository.save(currentUser);
                return new ResultMessage(true, "Edited");
            }
            return new ResultMessage(false, "User not found");
        }
        return new ResultMessage(false, "Email is already taken");
    }

    @Override
    public ResultMessage deleteUser(String id) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setIsDeleted(true);
            userRepository.save(user);
            return new ResultMessage(true, "Deleted");
        }
        return new ResultMessage(false, "User not found");

    }
}
