package uz.pdp.codingbat.service;

import uz.pdp.codingbat.entity.Category;
import uz.pdp.codingbat.entity.Language;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

public interface LanguageService {
    ResultMessage add(Language language);
    Language show(Integer id);
    List<Language> showAll();
    ResultMessage edit(Integer id, Language language);
    ResultMessage delete(Integer id);
}
