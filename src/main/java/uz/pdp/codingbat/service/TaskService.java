package uz.pdp.codingbat.service;

import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

public interface TaskService {

    ResultMessage add(Task task);
    Task show(Integer id);
    List<Task> showAll();
    List<Task> getAllTasksByCategory(Integer id);
    ResultMessage edit(Integer id,Task task);
    ResultMessage delete(Integer id);


}
