package uz.pdp.codingbat.service;

import uz.pdp.codingbat.dto.TestCaseDTO;
import uz.pdp.codingbat.entity.TestCase;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;

public interface TestCaseService {
    ResultMessage createTestCase(TestCaseDTO testCaseDTO);

    TestCase getTestCase(Integer testCaseId);

    List<TestCase> getActiveTestCases();

    List<TestCase> getDeletedTestCases();

    List<TestCase> getTestCaseBelongTask(Integer taskId);

    ResultMessage updateTestCase(TestCaseDTO testCaseDTO,Integer tesCaseId);

    ResultMessage deleteTestCase(Integer tesCaseId);

}
