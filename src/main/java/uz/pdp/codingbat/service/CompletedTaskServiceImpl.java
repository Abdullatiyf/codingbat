package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.codingbat.dto.CompletedTaskDTO;
import uz.pdp.codingbat.entity.CompletedTask;
import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.repository.CompletedTaskRepository;
import uz.pdp.codingbat.repository.TaskRepository;
import uz.pdp.codingbat.repository.UserRepository;
import uz.pdp.codingbat.util.ResultMessage;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CompletedTaskServiceImpl implements CompletedTaskService {
    final CompletedTaskRepository completedTaskRepository;
    final UserRepository userRepository;
    final TaskRepository taskRepository;

    @Override
    public List<CompletedTask> getAllCompletedTasks() {
        return completedTaskRepository.getAllCompletedTask();
    }

    @Override
    public CompletedTask getCompletedTask(Integer id) {
        return completedTaskRepository.findByIsDeletedFalseAndId(id);
    }

    @Override
    public List<CompletedTask> getAllByIsComplete() {
        return completedTaskRepository.getAllByIsComplete();
    }

    @Override
    public ResultMessage create(CompletedTaskDTO completedTaskDTO) {
        Optional<User> optionalUser = userRepository.findById(completedTaskDTO.getUser().getId());
        if (optionalUser.isEmpty()) return new ResultMessage(false, "User not found");

        Optional<Task> optionalTask = taskRepository.findById(completedTaskDTO.getTask().getId());
        if (optionalTask.isEmpty()) return new ResultMessage(false, "Task not found");

        CompletedTask completedTask = CompletedTask.builder()
                        .answerText(completedTaskDTO.getAnswerText())
                        .isDeleted(false)
                        .isCompleted(false)
                        .testCaseCount(completedTaskDTO.getTestCaseCount())
                        .successTestCaseCount(completedTaskDTO.getSuccessTestCaseCount())
                        .task(optionalTask.get())
                        .users(optionalUser.get())
                        .date(LocalDateTime.now())
                        .build();
        completedTaskRepository.save(completedTask);
        return new ResultMessage(true, "Created");
    }

    @Override
    public ResultMessage update(Integer id, CompletedTaskDTO completedTaskDTO) {
        Optional<User> optionalUser = userRepository.findById(completedTaskDTO.getUser().getId());
        if (optionalUser.isEmpty()) return new ResultMessage(false, "User not found");

        Optional<Task> optionalTask = taskRepository.findById(completedTaskDTO.getTask().getId());
        if (optionalTask.isEmpty()) return new ResultMessage(false, "Task not found");

        Optional<CompletedTask> optionalCompletedTasks = completedTaskRepository.findById(id);
        if (optionalCompletedTasks.isEmpty()) return new ResultMessage(false, "Completed task not found");

        CompletedTask completedTask1 = optionalCompletedTasks.get();
        completedTask1.setTask(optionalTask.get());
        completedTask1.setIsCompleted(completedTaskDTO.getIsCompleted());
        completedTask1.setAnswerText(completedTaskDTO.getAnswerText());
        completedTask1.setTestCaseCount(completedTaskDTO.getTestCaseCount());
        completedTask1.setSuccessTestCaseCount(completedTaskDTO.getSuccessTestCaseCount());
        completedTask1.setUsers(optionalUser.get());

        completedTaskRepository.save(completedTask1);
        return new ResultMessage(true, "Edited");
    }

    @Override
    public ResultMessage delete(Integer id) {
        Optional<CompletedTask> optionalCompletedTasks = completedTaskRepository.findById(id);
        if (optionalCompletedTasks.isPresent()){
//            completedTaskRepository.blockById(id);
            CompletedTask completedTask = optionalCompletedTasks.get();
            completedTask.setIsDeleted(true);
            completedTaskRepository.save(completedTask);
            return new ResultMessage(true, "Deleted");
        }
        return new ResultMessage(false, "Not found");
    }
}
