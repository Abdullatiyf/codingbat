package uz.pdp.codingbat.service;

import uz.pdp.codingbat.dto.CompletedTaskDTO;
import uz.pdp.codingbat.entity.CompletedTask;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;


public interface CompletedTaskService {

    List<CompletedTask> getAllCompletedTasks();

    CompletedTask getCompletedTask(Integer id);

    List<CompletedTask> getAllByIsComplete();

    ResultMessage create(CompletedTaskDTO completedTaskDTO);

    ResultMessage update(Integer id, CompletedTaskDTO completedTaskDTO);

    ResultMessage delete(Integer id);
}
