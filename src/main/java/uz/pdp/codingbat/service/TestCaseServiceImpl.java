package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.codingbat.dto.TestCaseDTO;
import uz.pdp.codingbat.entity.Task;
import uz.pdp.codingbat.entity.TestCase;
import uz.pdp.codingbat.repository.TaskRepository;
import uz.pdp.codingbat.repository.TestCaseRepository;
import uz.pdp.codingbat.util.ContextHolder;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestCaseServiceImpl implements TestCaseService {
    final TaskRepository taskRepository;
    final TestCaseRepository testCaseRepository;

    @Override
    public ResultMessage createTestCase(TestCaseDTO testCaseDTO) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "you are not an admin");
        }
        Optional<Task> task = taskRepository.findById(testCaseDTO.getTaskId());
        if(task.isPresent()){
            TestCase testCase = TestCase.builder()
                    .caseText(testCaseDTO.getCaseText())
                    .task(task.get())
                    .isDeleted(false)
                    .build();
            testCaseRepository.save(testCase);
            return new ResultMessage(true,"TestCase created");
        }
        return new ResultMessage(false, "Task not found");
    }

    @Override
    public TestCase getTestCase(Integer testCaseId) {
        return testCaseRepository.findById(testCaseId).orElse(null);
    }

    @Override
    public List<TestCase> getActiveTestCases() {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ArrayList<>();
        }
        return testCaseRepository.findAllByIsDeletedFalse();
    }

    @Override
    public List<TestCase> getDeletedTestCases() {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ArrayList<>();
        }
        return testCaseRepository.findAllByIsDeletedTrue();
    }

    @Override
    public List<TestCase> getTestCaseBelongTask(Integer taskId) {
        Optional<Task> optionalTask = taskRepository.findById(taskId);
        if (optionalTask.isPresent()){
            return testCaseRepository.getTestCasesByTaskId(taskId);
        }
        return new ArrayList<>();
    }

    @Override
    public ResultMessage updateTestCase(TestCaseDTO testCaseDTO,Integer testCaseId) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "you are not an admin");
        }
        Optional<Task> optionalTask = taskRepository.findById(testCaseDTO.getTaskId());
        if (optionalTask.isPresent()){
            Optional<TestCase> optionalTestCase = testCaseRepository.findById(testCaseId);
            if (optionalTestCase.isPresent()){
                TestCase testCase = optionalTestCase.get();
                testCase.setTask(optionalTask.get());
                testCase.setCaseText(testCaseDTO.getCaseText());
                testCaseRepository.save(testCase);
                return new ResultMessage(true, "Edited");
            }
            return new ResultMessage(false, "Test case not found");
        }
        return new ResultMessage(false, "Task not found");
    }

    @Override
    public ResultMessage deleteTestCase(Integer testCaseId) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "you are not an admin");
        }
        Optional<TestCase> tesCaseOptional = testCaseRepository.findById(testCaseId);
        if (tesCaseOptional.isPresent()) {
//            testCaseRepository.deleteTestCasesById(testCaseId);
            TestCase testCase = tesCaseOptional.get();
            testCase.setIsDeleted(true);
            testCaseRepository.save(testCase);
            return new ResultMessage(true,"Deleted");
        }
        return new ResultMessage(false,"Test case not found");
    }
}
