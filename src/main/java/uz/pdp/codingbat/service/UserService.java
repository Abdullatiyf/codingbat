package uz.pdp.codingbat.service;

import uz.pdp.codingbat.entity.User;
import uz.pdp.codingbat.dto.UserDTO;
import uz.pdp.codingbat.util.ResultMessage;

public interface UserService {

    UserDTO signUp(User user);

    ResultMessage validateUser(String email, String id, String code);

    ResultMessage checkUser(User user);

    UserDTO showUser(String id);

    UserDTO showUsers();

    ResultMessage editUser(String id, User user);

    ResultMessage deleteUser(String id);
}
