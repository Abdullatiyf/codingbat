package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.codingbat.entity.Category;
import uz.pdp.codingbat.entity.Language;
import uz.pdp.codingbat.repository.CategoryRepository;
import uz.pdp.codingbat.repository.LanguageRepository;
import uz.pdp.codingbat.util.ContextHolder;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {

    final CategoryRepository categoryRepository;
    final LanguageRepository languageRepository;

    @Override
    public ResultMessage add(Category category) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Category checkCategory = checkCategory(category.getName());
        if (Objects.nonNull(checkCategory)){
            return new ResultMessage(false, "Category added before!!!");
        }
        Optional<Language> optionalLanguage = languageRepository.findById(category.getLanguage().getId());
        if (optionalLanguage.isPresent()) {
            category.setIsDeleted(false);
            category.setLanguage(optionalLanguage.get());
            categoryRepository.save(category);
            return new ResultMessage(true, "Category saved");
        }
        return new ResultMessage(false, "language not found");
    }

    private Category checkCategory(String categoryName) {
        List<Category> categories = categoryRepository.findAll();
        return categories.stream().filter(category -> Objects.equals(category.getName(), categoryName))
                .findAny().orElse(null);
    }

    @Override
    public Category show(Integer id) {
        Optional<Category> byId = categoryRepository.findById(id);
        return byId.orElse(null);
    }

    @Override
    public List<Category> showAll() {
        return categoryRepository.findAll();
    }

    @Override
    public List<Category> getAllByLanguage(Integer id) {
        return categoryRepository.findAllByIsDeletedFalseAndLanguage_Id(id);
    }

    @Override
    public ResultMessage edit(Integer id, Category category) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        Optional<Language> optionalLanguage = languageRepository.findById(category.getLanguage().getId());
        if (optionalCategory.isPresent()) {
            if (optionalLanguage.isPresent()) {
                Category category1 = optionalCategory.get();
                category1.setName(category.getName());
                category1.setDescription(category.getDescription());
                category1.setStarCount(category.getStarCount());
                category1.setLanguage(optionalLanguage.get());
                categoryRepository.save(category1);
                return new ResultMessage(true, "edited");
            }
            return new ResultMessage(false, "Language not found");
        }
        return new ResultMessage(false, "Cannot find category with id " + id);
    }

    @Override
    public ResultMessage delete(Integer id) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "Permission denied");
        }
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isPresent()){
            Category category = optionalCategory.get();
            category.setIsDeleted(true);
            categoryRepository.save(category);
            return new ResultMessage(true, "deleted");
        }
       return new ResultMessage(false, "Category not found");
    }
}
