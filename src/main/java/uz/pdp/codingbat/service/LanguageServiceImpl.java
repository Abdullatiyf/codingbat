package uz.pdp.codingbat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.codingbat.entity.Language;
import uz.pdp.codingbat.repository.LanguageRepository;
import uz.pdp.codingbat.util.ContextHolder;
import uz.pdp.codingbat.util.ResultMessage;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LanguageServiceImpl implements LanguageService{
    final LanguageRepository languageRepository;
    @Override
    public ResultMessage add(Language language) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "you are not an admin");
        }
        Language checkLanguage = checkLanguage(language.getName());
        if (Objects.nonNull(checkLanguage)){
            return new ResultMessage(false, "Language added before!!!");
        }
        language.setIsDeleted(false);
        languageRepository.save(language);
        return new ResultMessage(true, "Saved");
    }

    private Language checkLanguage(String languageName) {
        List<Language> languages = languageRepository.findAll();
        return languages.stream().filter(language1 -> Objects.equals(language1.getName(), languageName))
                .findAny().orElse(null);
    }

    @Override
    public Language show(Integer id) {
        return languageRepository.findById(id).orElse(null);
    }

    @Override
    public List<Language> showAll() {
        return languageRepository.findAll();
    }

    @Override
    public ResultMessage edit(Integer id, Language language) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "you are not an admin");
        }
        Optional<Language> optionalLanguage = languageRepository.findById(id);
        if (optionalLanguage.isPresent()){
            Language language1 = optionalLanguage.get();
            language1.setName(language.getName());
            languageRepository.save(language1);
            return new ResultMessage(true, "Edited");
        }
        return new ResultMessage(false, "language not found");
    }

    @Override
    public ResultMessage delete(Integer id) {
        if (!Objects.equals(ContextHolder.user.getRole(), "admin")) {
            return new ResultMessage(false, "you are not an admin");
        }
        Optional<Language> optionalLanguage = languageRepository.findById(id);
        if (optionalLanguage.isPresent()){
//            languageRepository.blockById(id);
            Language language = optionalLanguage.get();
            language.setIsDeleted(true);
            languageRepository.save(language);
            return new ResultMessage(true, "Deleted");
        }
        return new ResultMessage(false, "Language not found");
    }
}
