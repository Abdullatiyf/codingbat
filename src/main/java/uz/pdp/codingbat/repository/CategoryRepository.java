package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.codingbat.entity.Category;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category,Integer> {
    @Query(nativeQuery = true,value = "update category set is_deleted = true where id = ?1")
    void blockById(Integer id);

    List<Category> findAllByIsDeletedFalseAndLanguage_Id(Integer id);
}
