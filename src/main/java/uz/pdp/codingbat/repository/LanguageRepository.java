package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.codingbat.entity.Language;

public interface LanguageRepository extends JpaRepository<Language,Integer> {

    @Modifying(clearAutomatically = true)
    @Query("update Language l set l.isDeleted = true where l.id =:id")
    void blockById(@Param("id") Integer id);
}
