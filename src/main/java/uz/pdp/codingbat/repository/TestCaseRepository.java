package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.codingbat.entity.TestCase;
import uz.pdp.codingbat.entity.User;

import java.util.List;

public interface TestCaseRepository extends JpaRepository<TestCase,Integer> {

    List<TestCase> findAllByIsDeletedFalse();

    List<TestCase> findAllByIsDeletedTrue();

    @Query(nativeQuery = true,value = "select * from test_case where is_deleted = false and task_id = ?1")
    List<TestCase> getTestCasesByTaskId(Integer id);
}
