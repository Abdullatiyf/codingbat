package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.codingbat.entity.CompletedTask;

import java.util.List;

public interface CompletedTaskRepository extends JpaRepository<CompletedTask,Integer> {
    @Query(nativeQuery = true, value = "select * from completed_task c where c.is_deleted<>true and c.is_completed=true")
    List<CompletedTask> getAllByIsComplete();

    @Query(nativeQuery = true, value = "select * from completed_task c where c.is_deleted<>true")
    List<CompletedTask> getAllCompletedTask();

    @Query(nativeQuery = true, value = "update completed_task set is_deleted=true where id=?1")
    void blockById(Integer id);

    CompletedTask findByIsDeletedFalseAndId(Integer id);
}
