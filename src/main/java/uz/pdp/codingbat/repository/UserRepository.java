package uz.pdp.codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.codingbat.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {
    @Query(nativeQuery = true,value = "select * from users where id = ?1 and is_deleted = false;")
    Optional<User> findById(String id);
    @Query(nativeQuery = true,value = "select * from users where email = ?1 and password = ?2 and is_active = true;")
    Optional<User> checkUser(String email, String password);
    @Query(nativeQuery = true,value = "update users set is_deleted = true where id = ?1")
    void blockById(String id);

    List<User> findAllByIsDeletedFalse();
}
